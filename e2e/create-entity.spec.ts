import 'jest';
import { gql } from './util/gql-client';
import { startLocalServer, stopLocalServer } from './person'

describe('Create Entity', () => {
    beforeAll(async () => {
        await startLocalServer();
    });

    afterAll(async () => {
        await stopLocalServer();
    });

    let personId;
    it('Creates a new ID', async () => {
        const res = await gql(`
            mutation {
                addPerson(data: {
                    name:"Joe"
                    age: 40
                }) {
                    _id
                }
            }`
        );

        const body = res.data;
        personId = body.data.addPerson._id;
        expect(personId).toBeDefined();
    })

    it('Can be queried with new ID', async () => {
        const res = await gql(`
        query {
            Person(_id:"${personId}"){
              name
              age
            }
          }
            `
        );

        const body = res.data;
        expect(body.data.Person).not.toEqual(null)
        expect(body.data.Person.name).toEqual('Joe')
        expect(body.data.Person.age).toEqual(40)
    })

    it('Returns extra fields null', async () => {
        const res = await gql(`
        query {
            Person(_id:"${personId}"){
              name
              age
              address {
                  addressLineOne
              }
            }
          }
            `
        );

        const body = res.data;
        expect(body.data.Person).not.toEqual(null)
        expect(body.data.Person.address).toEqual(null)
    })

    it('Returns 400 when Query fields are not right', async () => {
        expect.assertions(2);
        await gql(`
        query {
            Person(_id:"${personId}"){
              undefinedFieldHere
              undefinedField2Here
              undefinedField3Here
              name
            }
          }
        `
        )
            .catch((err) => {
                expect(err.response.data.errors).toHaveLength(3); // 3 wrong fields, 3 errors
                expect(err.response.status).toEqual(400);
            })
    })
})