import 'jest';
import { gql } from './util/gql-client';
import { startLocalServer, stopLocalServer } from './person'
import { range } from 'lodash';

/**
 * Tests the ability to query multiple entities in a 'Page'
 */
describe('Query Entity Page', () => {
    beforeAll(async () => {
        await startLocalServer();
    });

    afterAll(async () => {
        await stopLocalServer();
    });

    it('Paginate on empty data', async () => {
        const res = await gql(`
            query {
                PersonPage(num: 100, offset: 0) {
                    _id
                    name
                    age
                }
            }`
        );

        const body = res.data;
        expect(body.data.PersonPage).toHaveLength(0);
    })

    it('Paginate on all data', async () => {

        // Add 5 people
        for(const i of range(5)) {
            await gql(`
                mutation {
                    addPerson(data: {
                        name:"Joe"
                        age: ${i}
                    }) {
                        _id
                    }
                }`
            );
        }

        const res = await gql(`
            query {
                PersonPage(num: 5, offset: 0) {
                    _id
                    age
                }
            }
        `)

        // Check there are 5 people, and the ages go from 0 to 4
        const body = res.data;
        expect(body.data.PersonPage).toHaveLength(5);

        const ages = body.data.PersonPage.map(p => p.age).sort();
        expect(ages).toEqual(range(5));
    })

    it('Paginate with subset', async () => {
        const res = await gql(`
            query {
                PersonPage(num: 2, offset: 0) {
                    _id
                    age
                }
            }
        `)

        const body = res.data;
        expect(body.data.PersonPage).toHaveLength(2);

        const ages = body.data.PersonPage.map(p => p.age).sort();
        expect(ages).toEqual([0,1]);
    })

    it('Paginate with subset and offset', async () => {
        const res = await gql(`
            query {
                PersonPage(num: 2, offset: 2) {
                    _id
                    age
                }
            }
        `)

        const body = res.data;
        expect(body.data.PersonPage).toHaveLength(2);

        const ages = body.data.PersonPage.map(p => p.age).sort();
        expect(ages).toEqual([2,3]);
    })

})