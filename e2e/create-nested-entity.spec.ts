import 'jest';
import { gql } from './util/gql-client';
import { startLocalServer, stopLocalServer } from './person'
import { range } from 'lodash';

/**
 * Tests the ability to query multiple entities in a 'Page'
 */
describe('Create Nested Entity', () => {
    let personId;
    beforeAll(async () => {
        await startLocalServer();

        // Create one person
            await gql(`
                mutation {
                    addPerson(data: {
                        name:"Joe"
                        age: 40
                    }) {
                        _id
                    }
                }`
            )
            .then((res) => {
                personId = res.data.data.addPerson._id
            })
    });

    afterAll(async () => {
        await stopLocalServer();
    });

    it('Create Address (single child)', async () => {
        await gql(`
            mutation {
                addAddress(personId: "${personId}", data: {
                    addressLineOne: "1234 Street Blvd"
                    addressLineTwo: "Suite 5678"
                    city: "Municipal"
                    state: "Province"
                    country: "Nation"
                    zipCode: "12345"
                }) {
                    _id
                }
            } 
        `
        );

        const res = await gql(`
            query {
                Person(_id: "${personId}") {
                    address {
                        addressLineOne
                    }
                }
            }
        `)

        expect(res.data.data.Person.address.addressLineOne).toEqual("1234 Street Blvd")
    })


    it('Create Cards (many children)', async () => {
        await gql(`
            mutation {
                addCard(personId: "${personId}", data: {
                    type: "Credit"
                }) {
                    _id
                }
            } 
        `);

        await gql(`
            mutation {
                addCard(personId: "${personId}", data: {
                    type: "Debit"
                }) {
                    _id
                }
            } 
        `);

        const res = await gql(`
            query {
                Person(_id: "${personId}") {
                    cards {
                        type
                    }
                }
            }
        `)

        expect(res.data.data.Person.cards).toEqual([
            {type: "Credit"},
            {type: "Debit"}
        ])
    })
})