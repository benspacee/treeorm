import axios from 'axios';

export function gql(query: string) {
    return axios({
        url:'http://localhost:3000/graphql', 
        method: 'POST',
        withCredentials: false, 
        data: {
            query,
        }
    }).catch(e => {
        if (e.response.data) {
            console.error(JSON.stringify(e.response.data));
        }
        throw e
    });
}