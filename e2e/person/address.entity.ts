import { Node } from "../../lib/decorators/tree-node.decorator";
import { Property } from "../../lib/decorators/property.decorator";
import { BaseEntity } from "../../lib/db/interface/base-entity";

@Node('Address')
export class Address extends BaseEntity {
    @Property()
    addressLineOne: string;

    @Property()
    addressLineTwo: string;

    @Property()
    city: string;

    @Property()
    state: string;

    @Property()
    country: string;

    @Property()
    zipCode: string;
}