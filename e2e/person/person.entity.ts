import { Node } from "../../lib/decorators/tree-node.decorator";
import { Property } from "../../lib/decorators/property.decorator";
import { Child } from "../../lib/decorators/relations/child.decorator";
import { Address } from "./address.entity";
import { BaseEntity } from "../../lib/db/interface/base-entity";
import { Children } from "../../lib/decorators/relations/children.decorator";
import { Card } from "./card";

@Node('Person')
export class Person extends BaseEntity {
    @Property()
    name: string

    @Property({
        type: 'Int'
    })
    age: number

    @Child(Address)
    address: Address[];

    @Children(Card)
    cards: Card[];
}
