import { createSchema, } from "../../lib/schema/create-schema";
import { Person } from "./person.entity";
import { Address } from "./address.entity";
import * as express from 'express';
import { graphqlHTTP } from 'express-graphql';
import { InMemoryDb } from "../../lib/db/connectors/in-memory.database";
import { Card } from "./card";

let server;
export async function startLocalServer() {
    const schema = await createSchema(
        [
            Person,
            Address,
            Card,
        ],
        new InMemoryDb()
    );

    const app = express();
    app.use(
        '/graphql',
        graphqlHTTP(async (req, res, params) => {
            return {
                schema,
                graphiql: true,
                context: {
                    req,
                }
            }
        })
    )

    return new Promise((res, rej) => {
        server = app.listen(3000, (err) => {
            if (err) {
                return rej(err);
            }
            console.log('Listening on port http://localhost:3000/graphql')

            return res(null);
        })
    })
}

export function stopLocalServer() {
    return new Promise((res, rej) => {
        server.close((err) => {
            if (err) {
                return rej(err);
            }
            console.log('Server stopped')
            return res(null);
        });
    })
}
