import { Node } from "../../lib/decorators/tree-node.decorator";
import { Property } from "../../lib/decorators/property.decorator";
import { BaseEntity } from "../../lib/db/interface/base-entity";

@Node('Card')
export class Card extends BaseEntity {
    @Property()
    type: string; // Credit, Debit

    @Property()
    channel: string; // visa, mastercard, amex

    @Property()
    number: string; 

    @Property()
    expMonth: string; 

    @Property()
    expYear: string; 
}