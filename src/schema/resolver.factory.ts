import { ObjectTypeComposerFieldConfigAsObjectDefinition, ResolverDefinition } from "graphql-compose";
import { TreeOrmDatabase } from "../db/interface/treeorm-database.interface";
import { MetadataStore } from "../metadata/metadata-store";
import { ChildMetadata, EntityMetadata } from "../types/types";

/**
 * Utility for building all the resolvers defined on an entity during Schema Creation
 */
export class ResolverFactory {
    constructor(
        private entity: EntityMetadata,
        private database: TreeOrmDatabase,
    ) { }

    findOne(): ResolverDefinition<any, any> {
        return {
            name: 'findOne',
            args: { _id: 'String!' },
            type: this.entity.name,
            resolve: async ({ source, args }) => {
                const repository = await this.database.getRepository(this.entity.name)
                return (await repository).findOne(args._id);
            }
        }
    }

    findMany(): ResolverDefinition<any, any> {
        return {
            name: 'findMany',
            args: { num: 'Int', offset: 'Int' },
            type: `[${this.entity.name}]`,
            resolve: async ({ source, args }) => {
                const repository = await this.database.getRepository(this.entity.name)
                const data = await repository.find();
                return data.slice(args.offset, args.offset + args.num);
            }

        }
    }

    /**
     * The resolver called on all CreateEntity operations.
     * Creates a new instance of this entity in the DB, and links it to its parent if needed.
     */
    create(): ResolverDefinition<any, any> {
        const args: any = {} // Args for the createEntity mutation

        // If this entity has a parent, add the parentId as the first arg
        const parentMetadata = this.entity.parent && MetadataStore.getEntityMetadata(this.entity.parent);
        const parentIdArg = parentMetadata && `${parentMetadata.name.toLowerCase()}Id`;
        if (parentMetadata) {
            args[parentIdArg] = "String!"
        }

        // NOTE: the EntityInput TypeComposer must already be defined in the schema. See schema.factory.ts for more
        args.data = `${this.entity.name}Input`

        return {
            name: 'create',
            type: `${this.entity.name}`,
            args,
            resolve: async ({ source, args }) => {
                const repository = await this.database.getRepository(this.entity.name);
                const insertData = {
                    ...args.data,
                    __type: this.entity.name,
                };
                const entity = await repository.insert(insertData);

                // Update the parent if there is one
                if (!!parentMetadata) {
                    const parentRepo = await this.database.getRepository(parentMetadata.name);
                    const parentEntity = await parentRepo.findOne(args[parentIdArg]);
                    if (!parentEntity) {
                        return null; // TODO: Return a meaningful error message 'Parent not found'
                    }

                    // Update the child value in the parent entity
                    const childRelation = parentMetadata.children.find(c => c.typeName == this.entity.name);
                    if (childRelation.multiplicity == 'single') {
                        parentEntity[childRelation.name] = entity._id;
                    }

                    if (childRelation.multiplicity == 'many') {
                        parentEntity[childRelation.name] = [].concat(parentEntity[childRelation.name], entity._id).filter(i => !!i);
                    }

                    await parentRepo.update(parentEntity._id, parentEntity);
                }
                return entity;
            }
        }
    }


    /**
     * The nested resolver used to retrieve child entities when queried through GQL.
     * Here: 'source' is the parent object which the child belongs to
     *
     * @param childRelation
     */
    child(childRelation: ChildMetadata): ObjectTypeComposerFieldConfigAsObjectDefinition<any, any> {
        const childEntity = MetadataStore.getEntityMetadata(childRelation.type);
        const multiplicity = childRelation.multiplicity;

        return {
            type: (multiplicity == 'single') ? childEntity.name : `[${childEntity.name}]`,
            resolve: async (source, args) => {
                const repository = await this.database.getRepository(childEntity.name);

                if (!source[childRelation.name]) {
                    return null;
                }

                // In the DB, children objects are stored as reference IDs, using the same parameter name.
                // In this resolver, we just replace the ID with the entire child object.
                const ids = [].concat(source[childRelation.name]);
                return (multiplicity == 'single') ?
                    repository.findOne(ids[0])
                    :
                    Promise.all(
                        ids.map(id => repository.findOne(id))
                    );
            }
        }
    }
}