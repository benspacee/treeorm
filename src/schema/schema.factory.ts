import { GraphQLSchema } from "graphql";
import { ObjectTypeComposer, schemaComposer } from "graphql-compose";
import * as _ from 'lodash';
import { TreeOrmDatabase } from "../db/interface/treeorm-database.interface";
import { EntityMetadata } from "../types/types";
import { ResolverFactory } from './resolver.factory';

/**
 * Factory pattern for building GQL Schemas using the graphql-compose library.
 */
export class SchemaFactory {
    private entities: EntityMetadata[];
    private database: TreeOrmDatabase;

    public setEntities(entities) {
        this.entities = entities;
        return this;
    }
    public setDatabase(database) {
        this.database = database;
        return this;
    }

    private typeComposers: Record<string, ObjectTypeComposer> = {};

    public build(): GraphQLSchema {
        this.createTypeComposers();
        this.createQueries();
        this.createMutations();

        return schemaComposer.buildSchema();
    }

    /**
     * Create GQL type definitions for all the registered Entities.
     * Also creates relationship definitions, and resolvers for child nodes.
     * 
     * @param entities
     * @param database
     */
    createTypeComposers() {
        for (const entity of this.entities) {
            this.typeComposers[entity.name] = this.createEntityTypeComposer(entity);
        }

        // Loop back through the entities, and define all children resolvers
        // This must be done after all Base Entities are defined, to avoid references to undefined types
        for (const entity of this.entities) {
            const tc: ObjectTypeComposer = this.typeComposers[entity.name];
            const resolverFactory = new ResolverFactory(entity, this.database);

            for (const childRelation of entity.children) {
                tc.addFields({
                    [childRelation.name]: resolverFactory.child(childRelation)
                })
            }
        }
    }

    /**
     * Creates the Graphql-Compose TypeComposer for a single entity.
     * And creates all the basic resolvers that might be needed for that entity.
     * @param entity 
     * @param database 
     */
    createEntityTypeComposer(entity: EntityMetadata) {
        const typeComposer = schemaComposer.createObjectTC({
            name: entity.name,
            fields: _.fromPairs(entity.properties.map(p => [
                p.name,
                p.type + (p.nullable ? '!' : '')
            ])),
        });

        // Create an 'EntityInput' type, used in create operations
        const fields = _.fromPairs(entity.properties.map(property => [
            property.name,
            property.type
        ]));
        schemaComposer.createInputTC({
            name: `${entity.name}Input`,
            fields,
        });

        // Resolvers handle query and mutation operations on a given Collection (type).
        // We define them all here, and later we will map specific functions (addEntity) to different resolvers
        const resolverFactory = new ResolverFactory(entity, this.database);
        typeComposer.addResolver(resolverFactory.findOne())
        typeComposer.addResolver(resolverFactory.findMany())
        typeComposer.addResolver(resolverFactory.create())

        return typeComposer;
    }

    /**
     * Registers a findOne and findAll query for each entity.
     *
     * @param entities
     * @param database
     */
    createQueries() {
        // TODO: Instead of looping all entities, use a separate @Query decorator to
        // define which entities get query functions, and what the query endpoints look like.
        for (const entity of this.entities) {
            schemaComposer.Query.addFields({
                // FindOne query, using the name of the entity
                [`${entity.name}`]: this.typeComposers[entity.name].getResolver('findOne'),

                // FindMany query, using the name of the entity + "Page". Takes 2 pagination arguments
                [`${entity.name}Page`]: this.typeComposers[entity.name].getResolver('findMany'),
            })
        }
    }

    /**
     * Registers a create mutation for each Entity
     *
     * @param entities
     * @param database
     */
    createMutations() {
        for (const entityMetadata of this.entities) {
            schemaComposer.Mutation.addFields({
                [`add${entityMetadata.name}`]: this.typeComposers[entityMetadata.name].getResolver('create')

                /* 
                TODO: 
                    - addMany Mutation
                    - update Mutation
                    - delete Mutation
                */
            })
        }
    }
}
