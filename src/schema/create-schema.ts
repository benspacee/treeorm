import { TreeOrmDatabase } from "../db/interface/treeorm-database.interface";
import { MetadataStore } from "../metadata/metadata-store";
import { ClassType, EntityMetadata } from "../types/types";
import { SchemaFactory } from './schema.factory';

/**
 * Given a list of treenode entities, create a GQL schema that can be served through
 * any GQL server.
 */
export async function createSchema(entities: ClassType<any>[], database: TreeOrmDatabase) {
    const metadata: EntityMetadata[] = entities.map(e => MetadataStore.getEntityMetadata(e));

    await database.initialize(metadata);

    const factory = new SchemaFactory()
        .setEntities(metadata)
        .setDatabase(database);

    return factory.build();
}