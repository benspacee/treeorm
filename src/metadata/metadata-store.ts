import { ClassType, PropertyMetadata, EntityMetadata, RelationMetadata } from "../types/types";

/**
 * Static metadata store used for gathering
 * entity relations, etc.
 * 
 * We use this instead of Reflect-Metadata, becuase entities are inherently singleton-like
 * and this allows us to use more complex datatypes (intead of basic key-value pairs)
 */
export class MetadataStore {
    private static entities = []
    private static properties: PropertyMetadata[] = [];
    private static relations: RelationMetadata[] = [];

    public static addEntity(entityName: string, constructor: ClassType<any>) {
        MetadataStore.entities.push({
            constructor: constructor,
            entityName,
        });

        Reflect.defineMetadata('EntityName', entityName, constructor);
    }

    public static addProperty(property: PropertyMetadata) {
        MetadataStore.properties.push(property);
    }

    /**
     * Handle entity inheritance. Take all the proeprties that are defined on the parent entity
     * and duplicate them into the child entity.
     *
     * @param childConstructor
     * @param parentConstructor
     */
    public static handleSuperclass(childConstructor: ClassType<any>, parentConstructor: ClassType<any>) {
        const parentProperties = MetadataStore.properties.filter(p => p.constructor == parentConstructor);

        for(const property of parentProperties) {
            this.addProperty({
                ...property,
                constructor: childConstructor
            })
        }
    }

    public static addRelation(relation: RelationMetadata) {
        MetadataStore.relations.push(relation);
    }

    /**
     * Retrieve all the metadata defined on a Class (using decorators, Reflect, etc)
     *
     * @param constructor
     */
    public static getEntityMetadata(constructor: ClassType<any>): EntityMetadata {
        const entityName = Reflect.getMetadata('EntityName', constructor);

        return {
            constructor,
            name: entityName,
            properties: MetadataStore.properties.filter(p => p.constructor == constructor),
            parent: MetadataStore.relations.filter(r => r.child == constructor)?.[0]?.parent,
            children: MetadataStore.relations.filter(r => r.parent == constructor)
                .map(r => ({
                    name: r.name,
                    type: r.child,
                    typeName: Reflect.getMetadata('EntityName', r.child),
                    multiplicity: r.multiplicity,
                }))
        }
    }
}
