import { customAlphabet } from 'nanoid';
import { EntityMetadata } from "../../types/types";
import { BaseEntity } from "../interface/base-entity";
import { TreeOrmDatabase, TreeOrmRepository } from "../interface/treeorm-database.interface";

const nanoid = customAlphabet('1234567890', 3);

export class InMemoryDb implements TreeOrmDatabase {

    private repositories: Record<string, TreeOrmRepository> = {};

    initialize(entityMetadata: EntityMetadata[]) {
        for (const metadata of entityMetadata) {
            // TODO: Check for entities of the same name
            this.repositories[metadata.name] = new InMemoryRepository<any>();
        }

        return Promise.resolve();
    }

    getRepository(entityName: string): Promise<TreeOrmRepository> {
        return Promise.resolve(this.repositories[entityName]);
    }
}

export class InMemoryRepository<T extends BaseEntity = BaseEntity> implements TreeOrmRepository {
    private data: T[] = [];

    insert(data: T): Promise<T> {
        const entity = {
            _id: nanoid(),
            ...data,
        }
        this.data.push(entity);
        return Promise.resolve(entity);
    }

    async update(id: string, data: T): Promise<T> {
        let entity = this.data.find(d => d._id == id)
        entity = {
            ...entity,
            ...data,
            _id: id,
        };
        return Promise.resolve(entity);
    }

    find(): Promise<T[]> {
        return Promise.resolve(this.data);
    }

    findOne(id: string): Promise<T> {
        return Promise.resolve(this.data.find(d => d._id == id));
    }
}
