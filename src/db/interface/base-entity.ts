import { Property } from "../../decorators/property.decorator";

export class BaseEntity {
    @Property()
    _id: string;
}