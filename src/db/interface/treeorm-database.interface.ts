import { ClassType, EntityMetadata } from "../../types/types";
import { BaseEntity } from "./base-entity";

/**
 * Interface that treeorm resolvers use for all DB Connections.
 * Any storage mechanism should implement basic CRUD operations using this interface.
 * 
 * Basic assumptions of all DB interactions:
 * - All nodes share the same ID-space (i.e. ID's are globally unique across all entity types)
 */
export interface TreeOrmDatabase {
    /**
     * Connect to the database, and initialize all the entities.
     *
     * @param entities
     */
    initialize(entityMetadata: EntityMetadata[]): Promise<void>;

    /**
     * Get the repository for a specific Entity class
     * @param type Entity class
     */
    getRepository(entityName: string): Promise<TreeOrmRepository>;
}

/**
 * Entrypoint for accessing data of a specific type.
 *
 * I.E. Mongodb Collection, SQL Table
 */
export interface TreeOrmRepository<T extends BaseEntity = BaseEntity> {
    insert(entity: T): Promise<T>;

    update(id: string, entity: T): Promise<T>;

    find(): Promise<T[]>;

    findOne(id: string): Promise<T>;
}