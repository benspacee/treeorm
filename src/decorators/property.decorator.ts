import { MetadataStore } from "../metadata/metadata-store";
import { ClassType } from "../types/types";

export interface PropertyOptions {
    type?: string;
    nullable?: boolean;
}

export function Property(options?: PropertyOptions) {

    return function (target: Object, key: string) {

        const givenType = options?.type;

        // http://blog.wolksoftware.com/decorators-metadata-reflection-in-typescript-from-novice-to-expert-part-4
        // NOTE: This only works for basic primitives. Any classes will appear as "Object"
        const assumedType: Function = Reflect.getMetadata("design:type", target, key);

        const type = givenType ?? assumedType?.name;
        if(!type) {
            console.error(`Type for property ${target}:${key} could not be determined`);
        }

        if (type == 'Number') {
            console.error(`TypeError for property ${target}:${key}. GraphQL does not support 'Number' types. Please specify either Int or Float`)
        }

        MetadataStore.addProperty({
            constructor: target.constructor as ClassType<any>,
            name: key,
            type: type,
            nullable: options?.nullable ?? false,
        });
    }
}