import { ClassType } from "../../types/types";
import { MetadataStore } from "../../metadata/metadata-store";

/**
 * Decorator to represent an array of child nodes (of the same type)
 * 
 * Example: A "Person" parent-node could have one or more "Order" nodes
 */
export function Children(
    childClassType: ClassType<any>,
) {
    return function (object: Object, key: string) {
        MetadataStore.addRelation({
            parent: object.constructor as ClassType<any>,
            child: childClassType,
            name: key,
            multiplicity: 'many'
        });
    }
}