import { ClassType } from "../../types/types";
import { MetadataStore } from "../../metadata/metadata-store";

/**
 * Decorator to represent a single child node of the given type.
 * 
 * Example: A "Person" parent-node would have a single "Address" node.
 */
export function Child(
    childClassType: ClassType<any>,
) {
    return function (object: Object, key: string) {
        MetadataStore.addRelation({
            parent: object.constructor as ClassType<any>,
            child: childClassType,
            name: key,
            multiplicity: 'single'
        });
    }
}
