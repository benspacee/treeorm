import 'reflect-metadata';
import { MetadataStore } from '../metadata/metadata-store';

/**
 * Decorator for TreeNode Entities. Captures metadata on the entity like
 * parent-child relationships.
 */
export function Node(name: string, options?: any) {
    return function (target) {
        MetadataStore.addEntity(name, target);

        const parentTarget = Object.getPrototypeOf(target); // Trick to retrieve the parent target. Will return "Object" if there is no parent
        MetadataStore.handleSuperclass(target, parentTarget); 
    }
}
