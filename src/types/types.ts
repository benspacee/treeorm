export type ClassType<T> = {
    new(...args: any[]): T;
}

/**
 * Defines a scalar value on a Treenode Entity.
 */
export interface PropertyMetadata {
    constructor: ClassType<any>; // The class that this property belongs to
    name: string; // Name of the property for access
    type: string; // Graphql type (String, Int, ...)
    nullable: boolean;
}

export interface EntityMetadata {
    constructor: ClassType<any>; // The class that this belongs to
    name: string;
    properties: PropertyMetadata[];
    children: ChildMetadata[]; // Child entities
    parent: ClassType<any>; // Parent entity. For top-level nodes, this will just be 'Object'

}
export interface ChildMetadata {
    name: string; // Name used to access the child entity
    type: ClassType<any>; // Constructor of the child entity class
    typeName: string; // Name fo the child entity class
    multiplicity: Multiplicity; // Single child vs Array of children
}

// Intermediate POJO used to create ChildMetadata objects
export interface RelationMetadata {
    parent: ClassType<any>;
    child: ClassType<any>;
    name: string;
    multiplicity: Multiplicity;
}

export type Multiplicity = 'single' | 'many';
